from vehicleSet import vehicleSet
import pandas as p
import tkinter
from tkinter import filedialog
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plot
import numpy as np
import scipy.stats as stat

f = '/Users/connorforsythe/Box/CMU/Data/NHTS Data/Csv/vehpub.csv'
f1 = 'testVehData.csv'
sf = 'SalesData.xlsx'

t = vehicleSet(f1, 2017, sf)


mean = [15, 1.5]
cvm = [[1,0],[0,1]]

testD = stat.multivariate_normal(mean = mean, cov = cvm)
# print(t.vehiclesStrict)
# print(len(t.vehiclesStrict))
# print(t.vehiclesNorm)
# print(len(t.vehiclesNorm))
ageSpace =range(2,41)
cvmtSpace = range(0,500000,1000)
set = t.vehiclesNorm
type = 1

hist = t.survival(ageSpace, cvmtSpace, set)
#hist2 = t.survivalType(ageSpace, cvmtSpace, set, type)
hist2 = t.survival(ageSpace, cvmtSpace, set)

exp = t.constructEstimated(testD)
print("Chi-Val - {}".format(t.getChiVal(testD)))

age = []
cvmt = []
count = []
weights = []

fig = plot.figure()
fig2 = plot.figure()
fig3 = plot.figure()
ax = fig.add_subplot(111, projection='3d')
ax2 = fig2.add_subplot(111, projection = '3d')
ax3 = fig3.add_subplot(111, projection = '3d')

for ag, d0 in hist.items():
    for cv, val in d0.items():
        if(val['count']>0):
            a = ag
            c = cv

            age.append(a)
            cvmt.append(c)
            count.append(val['count'])
            weights.append(val['weight'])
            #print("Age: {} CVMT: {} Count: {} Weight:{}".format(a,c,val['count'],val['weight']))



ax.scatter(age,cvmt,weights)
ax.set_xlabel("Age (Years)")
ax.set_ylabel("CVMT (00'000)")
ax.set_zlabel("Count")
ax.set_title("All Vehicles")

age = []
cvmt = []
count = []
weights = []

for ag, d0 in hist2.items():
    for cv, val in d0.items():
        if(val['count']>0):
            a = ag
            c = cv

            age.append(a)
            cvmt.append(c)
            count.append(val['count'])
            weights.append(val['weight'])
            #print("Age: {} CVMT: {} Count: {} Weight:{}".format(a,c,val['count'],val['weight']))


print(t.scaledSurvival(ageSpace,cvmtSpace,set))

ax2.scatter(age,cvmt,weights)


ax2.set_xlabel("Age (Years)")
ax2.set_ylabel("CVMT (00'000)")
ax2.set_zlabel("Statistically Weight Count")
ax2.set_title("Cars Only")


print(t.scaledSurvival(ageSpace,cvmtSpace,set))

hist3 = t.scaledSurvival(ageSpace,cvmtSpace,set)
hist3 = exp

age = []
cvmt = []
scale = []

for ag, d0 in hist3.items():
    for cv, val in d0.items():
        if(val>0):
            a = ag
            c = cv

            age.append(a)
            cvmt.append(c)
            scale.append(val)
            #print("Age: {} CVMT: {} Count: {} Weight:{}".format(a,c,val['count'],val['weight']))




ax3.scatter(age,cvmt,scale)


ax3.set_xlabel("Age (Years)")
ax3.set_ylabel("CVMT (00'000)")
ax3.set_title("Scaled")

print(t.sales)

#plot.show()