from vehicleSet import vehicleSet
import pandas as p
import tkinter
from tkinter import filedialog
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plot
import numpy as np
import scipy.stats as stat

f = 'SimulatedVehDatav1.csv'
f1 = '/Users/connorforsythe/Box/CMU/Data/NHTS Data/Csv/vehpub.csv'
sf = 'SalesData.xlsx'

t = vehicleSet(f, 2017, sf)

print(t.vehiclesNorm)
print(len(t.vehiclesNorm))
print(t.sales)

ageRange = range(2,41)
cvmtRange = range(0,500000,1000)
set = t.vehiclesNorm

t.survival(ageRange,cvmtRange,set)
t.scaledSurvival(ageRange, cvmtRange, set)
t.surfaceSurv()
t.scatSurvWeight()

print(t.surv)

#Diff Data
mean = [15, 100000]
cvm = [[9,0],[0,20000**2]]
dist = stat.multivariate_normal(mean=mean, cov=cvm)
mean = []
age = []
cvmt = []
vals = []

diff = t.constructDiff(dist)
print(diff)
exp = t.constructEstimated(dist)
print(exp)
#chi = t.constructChiMat(dist)

for a, d0 in exp.items():
    for c, val in d0.items():
        age.append(a)
        cvmt.append(c)
        vals.append(val)

data = {'x-label':'Age','y-label':'CVMT','z-label':'Chi Val'}
data['x'] =  age
data['y'] = cvmt
data['z'] = vals

#vehicleSet.scatter3(data)