import numpy as np
from vehicleSample import vehicleSample
from collections import OrderedDict
import scipy.stats as stat
from scipy.optimize import shgo, brute

vf0 = 'SimulatedVehDatav1.csv'
vf1 = 'SimulatedVehDataWork.xlsx'
vf2 = '/Users/connorforsythe/Box/CMU/Data/NHTS Data/Csv/vehpub.csv'

sf = 'SalesData.xlsx'

refYear = 2017

t0 = vehicleSample(vf0, sf, refYear)
# print(t0.mCVMTeCDF)
# print(t0.mCVMTseCDF)
# print(t0.mAGEseCDF)
# print(t0.mAGEeCDF)
r = t0.constructSECDF()

temp = {'x':[], 'y':[], 'z':[]}
newCDF = OrderedDict()
tDist = stat.multivariate_normal(mean = [15,100000], cov = [[9,0],[0, 20000**2]])
for a, v0 in r.items():
    newAge = a/10
    newCDF[newAge] = OrderedDict()
    for c, val in v0.items():
        temp['x'].append(a)
        temp['y'].append(c)
        x = [a,c]
        temp['z'].append(val) #val tDist.cdf(x=x


        newC = c/100000

        newCDF[newAge][newC] = val

        print("Age: {} | CVMT: {} | CDF: {}".format(a/10,c/100000,val))


print(newCDF)
vehicleSample.surface(temp)

def getDiff(x, cdf):
    mean = [x[0], x[1]]
    cov = [[x[2],x[3]],[x[3],x[4]]]

    if(x[2]*x[4]-x[3]**2<=0.000001):
        return 1e10

    dist = stat.multivariate_normal(mean =mean, cov=cov)

    ss = 0

    for a, d0 in cdf.items():
        for c, val in d0.items():
            u = [a,c]

            exp = dist.cdf(u)
            obs = val

            diff = exp-obs

            ss = ss+diff**2
    return ss


b = [(0,30),(0,200000),(.000001,30),(-5,5),(.000001,1e10)]

# res = shgo(getDiff, bounds=b, args=(r,))

x = []

print()