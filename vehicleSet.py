import pandas as p
import tkinter
from tkinter import filedialog
from scipy.optimize import minimize, shgo, brute
from scipy.interpolate import griddata
import numpy as np
from matplotlib import cm
import scipy.stats as stat
import matplotlib.pyplot as plot






class vehicleSet(object):

    def __init__(self, file, refYear, salesFile):
        self.file = file
        self.vehiclesStrict = None
        self.vehiclesNorm = None
        self.sales = None
        self.refYear = refYear
        self.salesFile = salesFile
        self.populateVehicles()
        self.populateSales()
        self.surv = None
        self.scaledSurv = None


    def populateVehicles(self):
        df = p.read_csv(self.file)
        hhID = df['HOUSEID'].tolist()
        vID = df['VEHID'].tolist()
        my = df['VEHYEAR'].tolist()
        age = df['VEHAGE'].tolist()
        make = df['MAKE'].tolist()
        model = df['MODEL'].tolist()
        fueltype = df['FUELTYPE'].tolist()
        type = df['VEHTYPE'].tolist()
        odom = df['OD_READ'].tolist()
        avmt = df['BESTMILE'].tolist()
        state = df['HHSTATE'].tolist()
        weight = df['WTHHFIN'].tolist()

        size = len(hhID)
        temp = {}
        temp1 = {}
        for i in range(0,size):
            if(self.testVal(hhID[i]) and self.testVal(vID[i]) and self.testVal(my[i]) and self.testVal(age[i]) and self.testVal(make[i]) and self.testVal(model[i]) and self.testVal(fueltype[i]) and self.testVal(type[i]) and self.testVal(odom[i]) and self.testVal(avmt[i]) and self.testVal(state[i]) and self.testVal(weight[i])):
                temp[(hhID[i],vID[i])] = {'my':my[i],'age':age[i],'make':make[i],'model':model[i],'fuelType':fueltype[i],'type':type[i],'odom':odom[i],'avmt':avmt[i],'state':state[i],'weight':weight[i]}

            if (self.testVal(hhID[i]) and self.testVal(vID[i]) and self.testVal(my[i]) and self.testVal(
                    age[i]) and self.testVal(type[i]) and self.testVal(odom[i]) and self.testVal(weight[i])):
                temp1[(hhID[i], vID[i])] = {'my': my[i], 'age': age[i],
                                            'type': type[i], 'odom': odom[i],
                                            'weight': weight[i]}

        self.vehiclesStrict = temp
        self.vehiclesNorm = temp1
    def histogram(self, ageRange, cvmtRange, set):

        ageList = list(ageRange)
        cvmtList = list(cvmtRange)

        aLen = len(ageList)
        cLen = len(cvmtList)

        temp = {}

        for i in range(0, aLen):
            temp[(ageList[i])] = {}
            for j in range(0, cLen):
                temp[ageList[i]][cvmtList[j]] = {'weight': 0, 'count': 0}

        for i in range(0, aLen):
            for j in range(0, cLen - 1):
                for key, el in set.items():
                    if (ageList[i] == el['age'] and el['odom'] >= cvmtList[j] and el['odom']<cvmtList[j+1]):
                        temp[ageList[i]][cvmtList[j]]['weight'] = temp[ageList[i]][cvmtList[j]]['weight'] + el['weight']
                        temp[ageList[i]][cvmtList[j]]['count'] = temp[ageList[i]][cvmtList[j]]['count'] + 1
        return temp

    def histogramType(self, ageRange, cvmtRange, set, type):

        ageList = list(ageRange)
        cvmtList = list(cvmtRange)

        aLen = len(ageList)
        cLen = len(cvmtList)

        temp = {}

        for i in range(0, aLen):
            temp[(ageList[i])] = {}
            for j in range(0, cLen):
                temp[ageList[i]][cvmtList[j]] = {'weight': 0, 'count': 0}

        for i in range(0, aLen):
            for j in range(0, cLen - 1):
                for key, el in set.items():
                    if (ageList[i] == el['age'] and el['odom'] >= cvmtList[j] and el['odom']<cvmtList[j+1] and type == el['type']):
                        temp[ageList[i]][cvmtList[j]]['weight'] = temp[ageList[i]][cvmtList[j]]['weight'] + el['weight']
                        temp[ageList[i]][cvmtList[j]]['count'] = temp[ageList[i]][cvmtList[j]]['count'] + 1
        return temp

    def survival(self, ageRange, cvmtRange, set):

        ageList = list(ageRange)
        cvmtList = list(cvmtRange)

        aLen = len(ageList)
        cLen = len(cvmtList)

        temp = {}

        for i in range(0,aLen):
            temp[(ageList[i])] = {}
            for j in range(0,cLen):
                temp[ageList[i]][cvmtList[j]] = {'weight':0, 'count':0}

        for i in range(0,aLen):
            for j in range(0,cLen-1):
                for key, el in set.items():
                    if(ageList[i]==el['age'] and el['odom']>=cvmtList[j]):
                        temp[ageList[i]][cvmtList[j]]['weight']=temp[ageList[i]][cvmtList[j]]['weight']+el['weight']
                        temp[ageList[i]][cvmtList[j]]['count']=temp[ageList[i]][cvmtList[j]]['count']+1
        self.surv = temp
        return temp

    def survivalType(self, ageRange, cvmtRange, set, type):

        ageList = list(ageRange)
        cvmtList = list(cvmtRange)

        aLen = len(ageList)
        cLen = len(cvmtList)

        temp = {}

        for i in range(0,aLen):
            temp[(ageList[i])] = {}
            for j in range(0,cLen):
                temp[ageList[i]][cvmtList[j]] = {'weight':0, 'count':0}

        for i in range(0,aLen):
            for j in range(0,cLen-1):
                for key, el in set.items():
                    if(ageList[i]==el['age'] and el['odom']>=cvmtList[j] and type == el['type']):
                        temp[ageList[i]][cvmtList[j]]['weight']=temp[ageList[i]][cvmtList[j]]['weight']+el['weight']
                        temp[ageList[i]][cvmtList[j]]['count']=temp[ageList[i]][cvmtList[j]]['count']+1

        self.surv = temp
        return temp


    def populateSales(self):
        f = self.salesFile
        df = p.read_excel(f)

        majType = df['Major Type'].tolist()
        minType = df['Minor Type'].tolist()
        my = df['MY'].tolist()
        prod = df['PROD'].tolist()

        num = len(majType)
        temp = {}
        for i in range(0, num):
            temp[(majType[i],minType[i])] = {}

        for i in range(0, num):
            temp[(majType[i],minType[i])][my[i]] = prod[i]

        self.sales = temp

    def scaledSurvival(self, ageRange, cvmtRange, set):
        k = ('All', 'All')
        sales = self.sales[k]
        refYear = self.refYear

        hist = self.survival(ageRange,cvmtRange,set)
        temp = {}
        for age, d0 in hist.items():
            temp[age] = {}
            for cv, el in d0.items():
                temp[age][cv] = el['weight']/sales[refYear-age]

        self.scaledSurv = temp
        return temp

    def constructDiff(self,dist):
        if(self.surv == None or self.sales == None):
            print("There is no survival/sales variable built out. Please run the necessary function.")
            return None
        else:
            lol = 0
            surv = self.surv

            exp = self.constructEstimated(dist)
            diff = {}
            for age, d0 in surv.items():
                diff[age] = {}
                for cv, val in d0.items():
                    if(not surv[age][cv]==0):
                        diff[age][cv] = surv[age][cv]['weight'] - exp[age][cv]

            return diff

    def constructChiMat(self,dist):
        if(self.surv == None or self.sales == None):
            print("There is no survival/sales variable built out. Please run the necessary function.")
            return None
        else:
            lol = 0
            surv = self.surv

            exp = self.constructEstimated(dist)
            chi = {}
            for age, d0 in surv.items():
                chi[age] = {}
                for cv, val in d0.items():
                    if(not surv[age][cv]==0):
                        diff = surv[age][cv]['weight'] - exp[age][cv]
                        chi[age][cv] = (diff**2)/exp[age][cv]

            return chi

    def getChiVal(self,dist):
        if(self.surv == None or self.sales == None):
            print("There is no survival/sales variable built out. Please run the necessary function.")
            return np.inf
        else:
            diff = self.constructDiff(dist)
            exp = self.constructEstimated(dist)

            chi = 0
            for age, d0 in diff.items():
                for cv, val in d0.items():
                    dval = diff[age][cv]
                    eval = exp[age][cv]
                    chi = chi+(dval**2)/eval
            return chi


    def constructEstimated(self,dist):
        sales = self.sales[('All', 'All')]
        surv = self.surv
        refYear = self.refYear

        debug = {'x-label':'Age','y-label':'CVMT','z-label':'Suvival'}
        ag = []
        cvmt = []
        surVals = []


        temp = {}
        temp1 = {}
        for age, d0 in surv.items():
            temp[age] = {}
            temp1[age] = {}
            for cv, val in d0.items():
                temp[age][cv] = 0
                temp1[age][cv] = 0

        for age, d0 in temp.items():
            for cv, val in d0.items():
                surVal = 1-dist.cdf(x=[age,cv])
                salesVal = sales[refYear-age]

                temp[age][cv] = surVal*salesVal
                temp1[age][cv] = surVal

                ag.append(age)
                cvmt.append(cv)
                surVals.append(surVal)

        debug['x'] = ag
        debug['y'] = cvmt
        debug['z'] = surVals

        #vehicleSet.scatter3(debug)
        return temp



    def scatSurvWeight(self):
        temp = {'x-label':'Age', 'y-label':'CVMT','z-label':'Stat. Weights'}
        age = []
        cvmt = []
        weights = []
        hist = self.surv

        for ag, d0 in hist.items():
            for cv, val in d0.items():
                if (val['count'] > 0):
                    a = ag
                    c = cv

                    age.append(a)
                    cvmt.append(c)
                    weights.append(val['weight'])
        temp['x'] = age
        temp['y'] = cvmt
        temp['z'] = weights

        vehicleSet.scatter3(temp)

    def scatSurvCount(self):
        temp = {'x-label': 'Age', 'y-label': 'CVMT', 'z-label': 'Counts'}
        age = []
        cvmt = []
        count = []
        hist = self.surv

        for ag, d0 in hist.items():
            for cv, val in d0.items():
                if (val['count'] > 0):
                    a = ag
                    c = cv

                    age.append(a)
                    cvmt.append(c)
                    count.append(val['weight'])
        temp['x'] = age
        temp['y'] = cvmt
        temp['z'] = count
        vehicleSet.scatter3(temp)

    def contourScaledSurv(self):
        ssurv = self.scaledSurv

        age = []
        cvmt = []
        sur = []

        temp = {}

        for ag, d0 in ssurv.items():
            for cv, val in d0.items():
                age.append(ag)
                cvmt.append(cv)
                sur.append(val)

        temp['x'] = age
        temp['y'] = cvmt
        temp['z'] = sur
        vehicleSet.contour(temp)


    def surfaceScaledSurv(self):
        ssurv = self.scaledSurv

        age = []
        cvmt = []
        sur = []

        temp = {}

        for ag, d0 in ssurv.items():
            for cv, val in d0.items():
                age.append(ag)
                cvmt.append(cv)
                sur.append(val)

        temp['x'] = age
        temp['y'] = cvmt
        temp['z'] = sur
        vehicleSet.surface(temp)

    def surfaceSurv(self):

        ssurv = self.surv
        print("surv:{}".format(ssurv))

        age = []
        cvmt = []
        sur = []

        temp = {}

        for ag, d0 in ssurv.items():
            for cv, val in d0.items():
                age.append(ag)
                cvmt.append(cv)
                sur.append(val['weight'])

        temp['x'] = age
        temp['y'] = cvmt
        temp['z'] = sur
        vehicleSet.surface(temp)




    @staticmethod
    def testVal(val):
        r = False

        if(isinstance(val,(int,float,complex))):
            if(val>=0):
                r = True

        if(isinstance(val,str)):
            if(not val.__contains__('X')):
                r = True
        return r
    @staticmethod
    def scatter3(data):
        fig = plot.figure()
        ax = fig.add_subplot(111, projection='3d')
        xlab = data['x-label']
        ylab = data['y-label']
        zlab = data['z-label']
        x = data['x']
        y = data['y']
        z = data['z']

        ax.scatter(x, y, z)
        ax.set_xlabel(xlab)
        ax.set_ylabel(ylab)
        ax.set_zlabel(zlab)
        plot.show()
    @staticmethod
    def contour(data):
        fig = plot.figure()
        ax = fig.add_subplot(111)
        # xlab = data['x-label']
        # ylab = data['y-label']
        # zlab = data['z-label']
        #print("ENTERING CONTOUR METHOD")
        x = data['x']
        y = data['y']
        z = data['z']

        maxX = np.max(x)
        minX = np.min(x)
        lenX = len(x)
        stepX = (maxX-minX)/lenX

        maxY = np.max(y)
        minY = np.min(y)
        lenY = len(y)
        stepY = (maxY-minY)/lenY

        xRange = np.arange(minX,maxX,1)
        yRange = np.arange(minY,maxY,1000)

        xi, yi = np.meshgrid(xRange,yRange)
        #print("Interpolating data")
        zi = griddata((x,y), z, (xi,yi), 'linear')
        #print("Building contour")
        fig1 = ax.contourf(xi,yi,zi)
        cbar = plot.colorbar(fig1)
        #print("Showing plot")
        plot.show()

    @staticmethod
    def surface(data):
        fig = plot.figure()
        ax = fig.gca(projection='3d')
        # xlab = data['x-label']
        # ylab = data['y-label']
        # zlab = data['z-label']
        # print("ENTERING CONTOUR METHOD")
        x = data['x']
        y = data['y']
        z = data['z']

        print("Z:{}".format(z))
        maxX = np.max(x)
        minX = np.min(x)
        lenX = len(x)
        stepX = (maxX - minX) / lenX

        maxY = np.max(y)
        minY = np.min(y)
        lenY = len(y)
        stepY = (maxY - minY) / lenY

        xRange = np.arange(minX, maxX, 1)
        yRange = np.arange(minY, maxY, 100)

        xi, yi = np.meshgrid(xRange, yRange)
        # print("Interpolating data")
        zi = griddata((x, y), z, (xi, yi), 'linear')
        # print("Building contour")
        fig1 = ax.plot_surface(xi, yi, zi, cmap=cm.seismic, vmin=0, vmax=np.max(z))
        fig.colorbar(fig1)
        #cbar = plot.colorbar(fig1)
        # print("Showing plot")
        plot.show()
