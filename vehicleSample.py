import pandas as p
import tkinter
from tkinter import filedialog
from scipy.optimize import minimize, shgo, brute
from scipy.interpolate import griddata
import numpy as np
from matplotlib import cm
import scipy.stats as stat
import matplotlib.pyplot as plot
from collections import OrderedDict
from mpl_toolkits.mplot3d import Axes3D






class vehicleSample(object):

    def __init__(self, vFile, sFile, refYear):
        self.vFile = vFile
        self.sFile = sFile
        self.refYear = refYear
        self.strict = False

        self.vehicles = None
        self.vehicleNum = None
        self.sales = None
        self.maxMY = -np.inf
        self.minMY = np.inf
        self.maxCVMT = -np.inf
        self.minCVMT = np.inf
        self.eCDF = None
        self.seCDF = None

        self.mCVMTeCDF = None
        self.mAGEeCDF = None
        self.mCVMTseCDF = None
        self.mAGEseCDF = None

        self.populateSales()
        self.populateVehicles()


        self.myRange = (self.minMY, self.maxMY)
        self.cvmtRange = (self.minCVMT, self.maxCVMT)
        self.cvmtNumBins = 500

        self.salesKey = ('All', 'All')

        self.maxCVMT = np.maximum(self.maxCVMT, 500000)

        self.constructMarginalCVMTeCDF()
        self.constructMarginalCVMTseCDF()
        self.constructMarginalAGEeCDF()

    def populateVehicles(self):
        f = self.vFile
        csv = '.csv'
        if(f.find(csv)>0):
            df = p.read_csv(f)
        else:
            df = p.read_excel(f)

        hhID = df['HOUSEID'].tolist()
        vID = df['VEHID'].tolist()
        my = df['VEHYEAR'].tolist()
        age = df['VEHAGE'].tolist()
        make = df['MAKE'].tolist()
        model = df['MODEL'].tolist()
        fueltype = df['FUELTYPE'].tolist()
        type = df['VEHTYPE'].tolist()
        odom = df['OD_READ'].tolist()
        avmt = df['BESTMILE'].tolist()
        state = df['HHSTATE'].tolist()
        weight = df['WTHHFIN'].tolist()

        size = len(hhID)
        r = {}
        strict = self.strict

        maxMY = -np.inf
        minMY = np.inf
        maxCVMT = -np.inf
        minCVMT = np.inf



        for i in range(0,size):
            tempKey = (hhID[i], vID[i])
            tempVeh = {'my': my[i],'age': age[i], 'make':make[i], 'model':model[i], 'fuel type':fueltype[i],
                       'type':type[i], 'odom':odom[i], 'avmt':avmt[i], 'state':state[i],'weight':weight[i]}
            if(vehicleSample.testVehicle(tempVeh, strict)):
                if(tempVeh['my']>maxMY):
                    maxMY = tempVeh['my']

                if(tempVeh['my']<minMY):
                    minMY = tempVeh['my']

                if(tempVeh['odom']>maxCVMT):
                    maxCVMT = tempVeh['odom']

                if(tempVeh['odom']<minCVMT):
                    minCVMT = tempVeh['odom']

                r[tempKey] = tempVeh


        self.maxCVMT = maxCVMT
        self.minCVMT = minCVMT

        if(maxMY<self.maxMY):
            self.maxMY = int(maxMY)

        if(minMY>self.minMY):
            self.minMY = int(minMY)

        self.vehicles = r
        self.vehicleNum = len(r)
        return r

    def populateSales(self):
        f = self.sFile
        df = p.read_excel(f)

        majType = df['Major Type'].tolist()
        minType = df['Minor Type'].tolist()
        my = df['MY'].tolist()
        prodThousands = df['Prod (000)'].tolist()
        prod = df['PROD'].tolist()

        num = len(majType)
        r = {}
        for i in range(0, num):

            r[(majType[i],minType[i])] = {}


        maxMY = self.maxMY
        minMY = self.minMY

        for i in range(0, num):
            if (not prodThousands[i] == '-'):
                if(my[i]<minMY):
                    minMY = my[i]

                if(my[i]>maxMY):
                    maxMY = my[i]

                r[(majType[i],minType[i])][my[i]] = prod[i]


        self.maxMY = maxMY
        self.minMY = minMY

        self.sales = r

    def getTotalWeight(self):

        temp = []
        for key,val in self.vehicles.items():
            temp.append(val['weight'])
        return np.sum(temp)

    def constructMarginalCVMTeCDF(self):
        start = np.min(self.cvmtRange)
        end = np.max(self.cvmtRange)

        bins = np.linspace(start, end, self.cvmtNumBins)
        r = {}

        vehicles = self.vehicles

        for bin in bins:
            rbin = int(np.ceil(bin))
            r[rbin] = 0

        for bin in bins:
            for key, val in vehicles.items():
                odom = val['odom']
                weight = val['weight']
                rbin = int(np.ceil(bin))
                if(odom<=rbin):
                    r[rbin] = r[rbin]+weight
        self.mCVMTeCDF = r
        return r


    def constructMarginalAGEeCDF(self):
        seCDF = self.constructMarginalAGEseCDF()
        refYear = self.refYear

        start = refYear - np.max(self.myRange)
        end = refYear - (np.min(self.myRange) - 1)

        ages = range(start, end)

        salesKey = self.salesKey
        sales = self.sales[salesKey]

        r = {}
        for age in ages:
            my = self.refYear-age
            r[age] = seCDF[age]*sales[my]

        self.mAGEeCDF = r
        return r



    def constructMarginalCVMTseCDF(self):
        start = np.min(self.cvmtRange)
        end = np.max(self.cvmtRange)


        bins = np.linspace(start, end, self.cvmtNumBins)
        r = {}

        vehicles = self.vehicles
        totalWeight = self.getTotalWeight()

        for bin in bins:
            rbin = int(np.ceil(bin))
            r[rbin] = 0

        for bin in bins:
            for key, val in vehicles.items():
                odom = val['odom']
                weight = val['weight']/totalWeight
                rbin = int(np.ceil(bin))
                if(odom<=rbin):
                    r[rbin] = r[rbin]+weight
        self.mCVMTseCDF = r
        return r

    def constructMarginalAGEseCDF(self):
        salesKey = self.salesKey

        refYear = self.refYear

        start = refYear - np.max(self.myRange)
        end = refYear - (np.min(self.myRange) - 1)

        bins = list(range(start, end))

        r = {}
        w = {}

        vehicles = self.vehicles

        sales = self.sales[salesKey]

        for bin in bins:
            r[bin] = 0
            w[bin] = 0

        for bin in bins:
            for key, val in vehicles.items():
                age = self.refYear - val['my']

                weight = val['weight']

                if (age == bin):
                    r[bin] = r[bin] + weight
                    w[bin] = w[bin] + weight

        lenBins = len(bins)

        for i in range(0,lenBins):
            bin = bins[i]
            my = self.refYear - bin
            mySales = sales[my]





            r[bin] = (mySales-r[bin])/mySales

            if(bin ==1):
                r[bin] = 0

            rest = True

            if(w[bin]==0 and r[bin]==1 and bin>1):
                for j in range(i+1, lenBins):
                    newBin = bins[j]

                    if(w[newBin]>0):
                        rest = False
                if(not rest):
                    r[bin] = r[bins[i-1]]





        self.mAGEseCDF = r
        return r


    def constructSECDF(self):
        lol = 0

        salesKey = self.salesKey
        sales = self.sales[salesKey]

        refYear = self.refYear

        ageStart = refYear - np.max(self.myRange)
        ageEnd = refYear - (np.min(self.myRange) - 1)

        cvmtStart = np.min(self.cvmtRange)
        cvmtEnd = np.max(self.cvmtRange)
        cvmtNumBins = self.cvmtNumBins

        ageRange = range(ageStart, ageEnd)
        cvmtRange = np.linspace(cvmtStart,cvmtEnd,cvmtNumBins)

        vehicles = self.vehicles

        ageWeights = {}
        for age in ageRange:
            ageWeights[age] = {'val':0, 'restZero':True}
            for key, val in vehicles.items():
                vehAge = self.refYear-val['my']
                if(vehAge==age):
                    ageWeights[age]['val'] = ageWeights[age]['val']+val['weight']

        for age in ageRange:
            for age2 in range(age,ageEnd):
                if(not(age2==age) and ageWeights[age2]['val']>0):
                    ageWeights[age]['restZero'] = False



        seCVMTgAGE = OrderedDict()
        for age in ageRange:
            seCVMTgAGE[age] = OrderedDict()
            for cvmt in cvmtRange:
                seCVMTgAGE[age][cvmt] = 0

        for age in ageRange:
            for cvmt in cvmtRange:
                for key, val in vehicles.items():
                    vehAge = self.refYear-val['my']
                    vehOdom = val['odom']
                    vehWeight = val['weight']
                    if(vehAge==age and vehOdom<=cvmt):
                        seCVMTgAGE[age][cvmt] = seCVMTgAGE[age][cvmt] + vehWeight

            if(ageWeights[age]['val']>0):
                for cvmt in cvmtRange:
                    seCVMTgAGE[age][cvmt] = seCVMTgAGE[age][cvmt]/ageWeights[age]['val']


        mAGEseCDF = self.constructMarginalAGEseCDF()

        r = OrderedDict()
        ageList = list(ageRange)
        ageLen = len(list(ageList))

        for i in range(0,ageLen):
            age = ageList[i]
            r[age] = OrderedDict()
            for cvmt in cvmtRange:
                r[age][cvmt] = mAGEseCDF[age]*seCVMTgAGE[age][cvmt]

        for i in range(0,ageLen):
            age = ageList[i]
            weight = ageWeights[age]['val']
            if(i>0):
                prevAge = ageList[i-1]

            if(weight==0 and i>0):
                for cvmt in cvmtRange:
                    r[age][cvmt] = r[prevAge][cvmt]

        self.seCDF = r
        return r














    @staticmethod
    def testVehicle(veh, strict):
        laxList = ['my', 'type', 'odom', 'weight']
        if(strict):
            for key, val in veh.items():
                if(not vehicleSample.testVehVal(val)):
                    return False
        else:
            for key in laxList:
                if(not vehicleSample.testVehVal(veh[key])):
                    return False
        return True


    @staticmethod
    def testVehVal(val):
        r = False

        if(isinstance(val,(int,float,complex))):
            if(val>=0):
                r = True

        if(isinstance(val,str)):
            if(not val.__contains__('X')):
                r = True
        return r

    @staticmethod
    def testSalesVal(val):

        r = False

        if(isinstance(val,(int,float,complex))):
            if(val>=0):
                r = True
        return r

    @staticmethod
    def scatter3(data):
        fig = plot.figure()
        ax = fig.add_subplot(111, projection='3d')
        xlab = data['x-label']
        ylab = data['y-label']
        zlab = data['z-label']
        x = data['x']
        y = data['y']
        z = data['z']

        ax.scatter(x, y, z)
        ax.set_xlabel(xlab)
        ax.set_ylabel(ylab)
        ax.set_zlabel(zlab)
        plot.show()
    @staticmethod
    def contour(data):
        fig = plot.figure()
        ax = fig.add_subplot(111)
        # xlab = data['x-label']
        # ylab = data['y-label']
        # zlab = data['z-label']
        #print("ENTERING CONTOUR METHOD")
        x = data['x']
        y = data['y']
        z = data['z']

        maxX = np.max(x)
        minX = np.min(x)
        lenX = len(x)
        stepX = (maxX-minX)/lenX

        maxY = np.max(y)
        minY = np.min(y)
        lenY = len(y)
        stepY = (maxY-minY)/lenY

        xRange = np.arange(minX,maxX,1)
        yRange = np.arange(minY,maxY,1000)

        xi, yi = np.meshgrid(xRange,yRange)
        #print("Interpolating data")
        zi = griddata((x,y), z, (xi,yi), 'linear')
        #print("Building contour")
        fig1 = ax.contourf(xi,yi,zi)
        cbar = plot.colorbar(fig1)
        #print("Showing plot")
        plot.show()

    @staticmethod
    def surface(data):
        fig = plot.figure()
        ax = fig.gca(projection='3d')
        # xlab = data['x-label']
        # ylab = data['y-label']
        # zlab = data['z-label']
        # print("ENTERING CONTOUR METHOD")
        x = data['x']
        y = data['y']
        z = data['z']

        maxX = np.max(x)
        minX = np.min(x)
        lenX = len(x)
        stepX = (maxX - minX) / lenX

        maxY = np.max(y)
        minY = np.min(y)
        lenY = len(y)
        stepY = (maxY - minY) / lenY

        xRange = np.arange(minX, maxX, 1)
        yRange = np.arange(minY, maxY, 100)

        xi, yi = np.meshgrid(xRange, yRange)
        # print("Interpolating data")
        zi = griddata((x, y), z, (xi, yi), 'linear')
        # print("Building contour")
        fig1 = ax.plot_surface(xi, yi, zi, cmap=cm.seismic, vmin=0, vmax=np.max(z))
        fig.colorbar(fig1)
        #cbar = plot.colorbar(fig1)
        # print("Showing plot")
        plot.show()
